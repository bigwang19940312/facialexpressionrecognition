/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionload;
    QAction *actioncloseCam;
    QWidget *centralWidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_Fear;
    QWidget *gridLayoutWidget_4;
    QGridLayout *gridLayout_Sadness;
    QWidget *gridLayoutWidget_5;
    QGridLayout *gridLayout_Disgust;
    QWidget *gridLayoutWidget_6;
    QGridLayout *gridLayout_Angry;
    QWidget *gridLayoutWidget_7;
    QGridLayout *gridLayout_Joy;
    QWidget *gridLayoutWidget_8;
    QGridLayout *gridLayout_3;
    QLabel *label;
    QMenuBar *menuBar;
    QMenu *menuload;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(2731, 1718);
        actionload = new QAction(MainWindow);
        actionload->setObjectName(QStringLiteral("actionload"));
        actioncloseCam = new QAction(MainWindow);
        actioncloseCam->setObjectName(QStringLiteral("actioncloseCam"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayoutWidget = new QWidget(centralWidget);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(1960, 30, 731, 401));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayoutWidget_2 = new QWidget(centralWidget);
        gridLayoutWidget_2->setObjectName(QStringLiteral("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(10, 40, 671, 401));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        gridLayoutWidget_3 = new QWidget(centralWidget);
        gridLayoutWidget_3->setObjectName(QStringLiteral("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(20, 950, 661, 451));
        gridLayout_Fear = new QGridLayout(gridLayoutWidget_3);
        gridLayout_Fear->setSpacing(6);
        gridLayout_Fear->setContentsMargins(11, 11, 11, 11);
        gridLayout_Fear->setObjectName(QStringLiteral("gridLayout_Fear"));
        gridLayout_Fear->setContentsMargins(0, 0, 0, 0);
        gridLayoutWidget_4 = new QWidget(centralWidget);
        gridLayoutWidget_4->setObjectName(QStringLiteral("gridLayoutWidget_4"));
        gridLayoutWidget_4->setGeometry(QRect(20, 500, 661, 371));
        gridLayout_Sadness = new QGridLayout(gridLayoutWidget_4);
        gridLayout_Sadness->setSpacing(6);
        gridLayout_Sadness->setContentsMargins(11, 11, 11, 11);
        gridLayout_Sadness->setObjectName(QStringLiteral("gridLayout_Sadness"));
        gridLayout_Sadness->setContentsMargins(0, 0, 0, 0);
        gridLayoutWidget_5 = new QWidget(centralWidget);
        gridLayoutWidget_5->setObjectName(QStringLiteral("gridLayoutWidget_5"));
        gridLayoutWidget_5->setGeometry(QRect(960, 950, 721, 461));
        gridLayout_Disgust = new QGridLayout(gridLayoutWidget_5);
        gridLayout_Disgust->setSpacing(6);
        gridLayout_Disgust->setContentsMargins(11, 11, 11, 11);
        gridLayout_Disgust->setObjectName(QStringLiteral("gridLayout_Disgust"));
        gridLayout_Disgust->setContentsMargins(0, 0, 0, 0);
        gridLayoutWidget_6 = new QWidget(centralWidget);
        gridLayoutWidget_6->setObjectName(QStringLiteral("gridLayoutWidget_6"));
        gridLayoutWidget_6->setGeometry(QRect(1950, 970, 741, 451));
        gridLayout_Angry = new QGridLayout(gridLayoutWidget_6);
        gridLayout_Angry->setSpacing(6);
        gridLayout_Angry->setContentsMargins(11, 11, 11, 11);
        gridLayout_Angry->setObjectName(QStringLiteral("gridLayout_Angry"));
        gridLayout_Angry->setContentsMargins(0, 0, 0, 0);
        gridLayoutWidget_7 = new QWidget(centralWidget);
        gridLayoutWidget_7->setObjectName(QStringLiteral("gridLayoutWidget_7"));
        gridLayoutWidget_7->setGeometry(QRect(1950, 480, 741, 371));
        gridLayout_Joy = new QGridLayout(gridLayoutWidget_7);
        gridLayout_Joy->setSpacing(6);
        gridLayout_Joy->setContentsMargins(11, 11, 11, 11);
        gridLayout_Joy->setObjectName(QStringLiteral("gridLayout_Joy"));
        gridLayout_Joy->setContentsMargins(0, 0, 0, 0);
        gridLayoutWidget_8 = new QWidget(centralWidget);
        gridLayoutWidget_8->setObjectName(QStringLiteral("gridLayoutWidget_8"));
        gridLayoutWidget_8->setGeometry(QRect(690, 210, 1241, 711));
        gridLayout_3 = new QGridLayout(gridLayoutWidget_8);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(1020, 50, 511, 131));
        QFont font;
        font.setPointSize(30);
        label->setFont(font);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 2731, 21));
        menuload = new QMenu(menuBar);
        menuload->setObjectName(QStringLiteral("menuload"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuload->menuAction());
        menuload->addAction(actionload);
        menuload->addAction(actioncloseCam);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        actionload->setText(QApplication::translate("MainWindow", "load", nullptr));
        actioncloseCam->setText(QApplication::translate("MainWindow", "closeCam", nullptr));
        label->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        menuload->setTitle(QApplication::translate("MainWindow", "load", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
