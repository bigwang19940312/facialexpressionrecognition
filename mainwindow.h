#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <QTimer>
#include<QProcess>
#include<QVBoxLayout>
#include <QtCharts/QChart>
#include <QtCharts/QChartGlobal>
#include <QtCharts/QValueAxis>
#include <QLineSeries>
#include<QSplineSeries>
#include "chartview.h"

QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QChart;
class QChartView;
QT_CHARTS_END_NAMESPACE
QT_CHARTS_USE_NAMESPACE

//using namespace cv;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void sleep(unsigned int s);
    QProcess *p;
    QWidget *m_widget;
    void updateData(float value);
    void updateDataSurprise(QList<float>value);
    void updateDataSadness(QList<float>value);
    void updateDataJoy(QList<float>value);
    void updateDataAngry(QList<float>value);
    void updateDataDisgust(QList<float>value);
    void updateDataFear(QList<float>value);

    void initChartNeutral();
    void initChartSurprise();
    void initChartJoy();
    void initChartAngry();
    void initChartDisgust();
    void initChartFear();
    void initSadness();
    float joy1;
    void init();
private slots:
    void readResult() ;
    void on_pic_clicked();
    void endp();
    void timerSlot();

signals:
    void end();


private:
    int time;
    QList<float>Neutral;
    QList<float>Surprise;
    QList<float>Joy;
    QList<float>Fear;
    QList<float>Disgust;
    QList<float>Sadness;
    QList<float>Angry;


    QLineSeries *series;
    QList<QLineSeries *>seriesJoy;
    QList<QLineSeries *>seriesAngry;
    QList<QLineSeries *>seriesSadness;
    QList<QLineSeries *>seriesFear;
    QList<QLineSeries *>seriesSurprise;
    QList<QLineSeries *>seriesDisgust;

    QChart *chart;//n
    QChart *chartjoy;
    QChart *chartfear;
    QChart *chartdisgust;
    QChart *chartsurprise;
    QChart *chartsadness;
    QChart *chartangry;

    Ui::MainWindow *ui;
    QWindow *m_window;
    QVBoxLayout *pLayout;
    QChartView *chartView;
    QChartView * chartViewseriesSurprise;
    QChartView * chartViewseriesJoy;
    QChartView * chartViewseriesFear;
    QChartView * chartViewseriesSadness;
    QChartView * chartViewseriesAngry;
    QChartView * chartViewseriesDisgust;

    QTimer  *timer;
    quint16 count;
    quint16 countseriesSurprise;
    IplImage* pFrame;
    CvCapture *cam;
};

#endif // MAINWINDOW_H
