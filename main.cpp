#include "mainwindow.h"
#include <QApplication>
#include<QDesktopWidget>
#include<QPalette>
#include<QDebug>
#include<qdir.h>
using namespace std;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QDesktopWidget *pDesk = QApplication::desktop();
    MainWindow w(pDesk);

    w.setAutoFillBackground(true);
    QPalette palette;
    qDebug()<<QDir::currentPath();
    w.setStyleSheet("background-color:rgb(0,0,0)");
    w.resize(pDesk->width(),pDesk->height());
    w.move((pDesk->width() - w.width()) / 2, (pDesk->height() - w.height()) / 2);
    w.show();
    return a.exec();

}
