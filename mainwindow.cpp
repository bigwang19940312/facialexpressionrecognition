#include"mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QDir>
#include <QFile>
#include<QFrame>
#include<QTime>
#include "cxcore.h"
#include <QProcess>
#include<algorithm>
#include<QWindow>
#include<QWidget>
#include <dos.h>
#include<Windows.h>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QChartView>


QTime t1,t2;
int state=0;
QProcess *p;

QTimer *timer;

QString list[54] ={"Joy","Love","Optimism","Admiration","Gratitude","Desire","Sincerity","vitality","Trust","Acceptance","Harmony","Serenity","Calm","Boastfulness","Pride","Angry","Bravery","Defiance","Contempt","Deceptiveness",
                 "Hate","Aggressiveness","Disgust","Insincerity","Annoyance","Envy","Suspicion","Pessimism","Disapproval","Conflict","Insult",
                 "Sadness","Fatigue","Neglect","Tension","Passiveness","Boredom","Distraction","Apprehension","Uneasiness","Remorse","Submission","Grievance","Shame","Depression",
                 "Fear","Embarrassed","Puzzlement","Cowardice","Awe","Surprise","Anticipation","Interest","Neutral"};
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    chart(new QChart)
{
    ui->setupUi(this);
    time=0;
    QObject::connect(ui->actioncloseCam,SIGNAL(triggered(bool)),this,SLOT(endp()));
    QObject::connect(ui->actionload,SIGNAL(triggered(bool)),this,SLOT(on_pic_clicked()));
    timer = new QTimer(this);
    timer->setInterval(60);
    cam = NULL;
    QPalette pe;
    pe.setColor(QPalette::WindowText,Qt::white);
    ui->label->setText("表情识别");
    ui->label->setPalette(pe);
    ui->label->setAlignment(Qt::AlignCenter);
    QFont font("Microsoft YaHei", 20, 80);
    ui->label->setFont(font);
    initChartSurprise();
    initChartNeutral();
    initChartJoy();
    initChartAngry();
    initChartDisgust();
    initChartFear();
    initSadness();
    init();
    connect(this, SIGNAL(end()), this, SLOT(timerSlot()));
}

MainWindow::~MainWindow()
{
    delete ui;
    cvReleaseCapture(&cam);
}
void MainWindow::init()
{
    chart->axisX()->setRange(0+time, 60+time);
    chartfear->axisX()->setRange(0+time, 60+time);
    chartsadness->axisX()->setRange(0+time, 60+time);
    chartsurprise->axisX()->setRange(0+time, 60+time);
    chartdisgust->axisX()->setRange(0+time, 60+time);
    chartjoy->axisX()->setRange(0+time, 60+time);
    chartangry->axisX()->setRange(0+time, 60+time);

}

void MainWindow::initChartSurprise()
{
    QLineSeries *seriesS;
    seriesS=new QLineSeries[3];
    for(int i=0; i<5; i++)
    {
        seriesSurprise.push_back(&seriesS[i]);
    }

    QChart *chart1=new QChart();
    chartsurprise=chart1;
    for(int i=0;i<3;i++)
    {
       chart1->addSeries(seriesSurprise[i]);
    }
    chart1->setTitle("Surprise大类 ");
    chart1->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->setBackgroundBrush(Qt::SolidPattern);
    chart1->createDefaultAxes();
    chart1->axisY()->setRange(0, 1);
    chart1->axisX()->setRange(0, 60);
    chart1->axisX()->setTitleBrush(QBrush(Qt::white));
    chart1->axisY()->setTitleBrush(QBrush(Qt::white));
    chart1->setTitleBrush(QBrush(Qt::white));
    QBrush axisBrush(Qt::white);
    chart1->axisX()->setLabelsBrush(axisBrush);
    chart1->axisY()->setLabelsBrush(axisBrush);
    chart1->axisX()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisY()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisX()->setTitleText("时间");
    chart1->axisY()->setTitleText("表情概率");
    chart1->axisX()->setGridLineVisible(false);
    chart1->axisY()->setGridLineVisible(false);
    /* hide legend */
    chart1->legend()->hide();
    chartViewseriesSurprise = new ChartView(chart1);
    chartViewseriesSurprise->setRenderHint(QPainter::Antialiasing);
    ui->gridLayout_2->addWidget(chartViewseriesSurprise);

}
void MainWindow::initChartNeutral()
{
    series = new QLineSeries;
    chart->addSeries(series);
    chart->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart->setTitle("Neutral大类");
    chart->setTitleBrush(QBrush(Qt::white));
    chart->createDefaultAxes();
    chart->setBackgroundBrush(Qt::SolidPattern);
    chart->axisY()->setRange(0, 1);
    chart->axisX()->setRange(0, 60);
    chart->axisX()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart->axisY()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart->axisX()->setTitleText("时间");
    chart->axisY()->setTitleText("表情概率");
    chart->axisX()->setTitleBrush(QBrush(Qt::white));
    chart->axisY()->setTitleBrush(QBrush(Qt::white));
    QBrush axisBrush(Qt::white);
    chart->axisX()->setLabelsBrush(axisBrush);
    chart->axisY()->setLabelsBrush(axisBrush);
    chart->axisX()->setGridLineVisible(false);
    chart->axisY()->setGridLineVisible(false);
    chart->legend()->hide();
    chartView = new ChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    ui->gridLayout->addWidget(chartView,0,65,40,40);
}

void MainWindow::initChartJoy()
{
    QLineSeries *seriesS;
    seriesS=new QLineSeries[15];
    for(int i=0; i<15; i++)
    {
        seriesJoy.push_back(&seriesS[i]);
    }

    QChart *chart1=new QChart();
    chartjoy=chart1;
    for(int i=0;i<15;i++)
    {
       chart1->addSeries(seriesJoy[i]);
    }

    chart1->setBackgroundBrush(Qt::SolidPattern);
    chart1->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));

    chart1->setTitle("Joy大类 ");
    chart1->createDefaultAxes();
    chart1->axisY()->setRange(0, 1);
    chart1->axisX()->setRange(0, 60);
    chart1->axisX()->setTitleBrush(QBrush(Qt::white));
    chart1->axisY()->setTitleBrush(QBrush(Qt::white));
    chart1->setTitleBrush(QBrush(Qt::white));

    QBrush axisBrush(Qt::white);
    chart1->axisX()->setLabelsBrush(axisBrush);
    chart1->axisY()->setLabelsBrush(axisBrush);
    chart1->axisX()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisY()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisX()->setTitleText("时间");
    chart1->axisY()->setTitleText("表情概率");

    chart1->axisX()->setGridLineVisible(false);
    chart1->axisY()->setGridLineVisible(false);
    chart1->legend()->hide();
    chartViewseriesJoy = new ChartView(chart1);
    chartViewseriesJoy->setRenderHint(QPainter::Antialiasing);
    ui->gridLayout_Joy->addWidget(chartViewseriesJoy);
}
void MainWindow::initChartAngry()
{
    QLineSeries *seriesS;
    seriesS=new QLineSeries[7];
    for(int i=0; i<7; i++)
    {
        seriesAngry.push_back(&seriesS[i]);
    }

    QChart *chart1=new QChart();
    chartangry=chart1;
    for(int i=0;i<7;i++)
    {
       chart1->addSeries(seriesAngry[i]);
    }

    chart1->setBackgroundBrush(Qt::SolidPattern);
    chart1->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));

    chart1->setTitle("Angry大类 ");
    chart1->createDefaultAxes();
    chart1->axisY()->setRange(0, 1);
    chart1->axisX()->setRange(0, 60);

    chart1->axisX()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisY()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisX()->setTitleText("时间");
    chart1->axisY()->setTitleText("表情概率");
    chart1->setTitleBrush(QBrush(Qt::white));
    chart1->axisX()->setTitleBrush(QBrush(Qt::white));
    chart1->axisY()->setTitleBrush(QBrush(Qt::white));
    QBrush axisBrush(Qt::white);
    chart1->axisX()->setLabelsBrush(axisBrush);
    chart1->axisY()->setLabelsBrush(axisBrush);
    chart1->axisX()->setGridLineVisible(false);
    chart1->axisY()->setGridLineVisible(false);
    chart1->legend()->hide();
    chartViewseriesAngry = new ChartView(chart1);
    chartViewseriesAngry->setRenderHint(QPainter::Antialiasing);
    ui->gridLayout_Angry->addWidget(chartViewseriesAngry);
}
void MainWindow::initChartDisgust()
{
    QLineSeries *seriesS;
    seriesS=new QLineSeries[9];
    for(int i=0; i<9; i++)
    {
        seriesDisgust.push_back(&seriesS[i]);
    }

    QChart *chart1=new QChart();
    chartdisgust=chart1;
    for(int i=0;i<9;i++)
    {
       chart1->addSeries(seriesDisgust[i]);
    }

    chart1->setBackgroundBrush(Qt::SolidPattern);
    chart1->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));

    chart1->setTitle("Disgust大类 ");
    chart1->createDefaultAxes();
    chart1->axisY()->setRange(0, 1);
    chart1->axisX()->setRange(0, 60);

    chart1->axisX()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisY()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisX()->setTitleText("时间");
    chart1->axisY()->setTitleText("表情概率");
    chart1->setTitleBrush(QBrush(Qt::white));
    chart1->axisX()->setTitleBrush(QBrush(Qt::white));
    chart1->axisY()->setTitleBrush(QBrush(Qt::white));
    QBrush axisBrush(Qt::white);
    chart1->axisX()->setLabelsBrush(axisBrush);
    chart1->axisY()->setLabelsBrush(axisBrush);
    chart1->axisX()->setGridLineVisible(false);
    chart1->axisY()->setGridLineVisible(false);
    chart1->legend()->hide();
    chartViewseriesDisgust = new ChartView(chart1);
    chartViewseriesDisgust->setRenderHint(QPainter::Antialiasing);
    ui->gridLayout_Disgust->addWidget(chartViewseriesDisgust);

}
void MainWindow::initChartFear()
{
    QLineSeries *seriesS;
    seriesS=new QLineSeries[5];
    for(int i=0; i<5; i++)
    {
        seriesFear.push_back(&seriesS[i]);
    }

    QChart *chart1=new QChart();
    chartfear=chart1;
    for(int i=0;i<5;i++)
    {
       chart1->addSeries(seriesFear[i]);
    }

    chart1->setBackgroundBrush(Qt::SolidPattern);
    chart1->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));

    chart1->setTitle("Fear大类 ");
    chart1->createDefaultAxes();
    chart1->axisY()->setRange(0, 1);
    chart1->axisX()->setRange(0, 60);

    chart1->axisX()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisY()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisX()->setTitleText("时间");
    chart1->axisY()->setTitleText("表情概率");
    chart1->setTitleBrush(QBrush(Qt::white));
    chart1->axisX()->setTitleBrush(QBrush(Qt::white));
    chart1->axisY()->setTitleBrush(QBrush(Qt::white));
    QBrush axisBrush(Qt::white);
    chart1->axisX()->setLabelsBrush(axisBrush);
    chart1->axisY()->setLabelsBrush(axisBrush);

    chart1->axisX()->setGridLineVisible(false);
    chart1->axisY()->setGridLineVisible(false);
    chart1->legend()->hide();

    chartViewseriesFear = new ChartView(chart1);
    chartViewseriesFear->setRenderHint(QPainter::Antialiasing);

    ui->gridLayout_Fear->addWidget(chartViewseriesFear);
}
void MainWindow::initSadness()
{
    QLineSeries *seriesS;
    seriesS=new QLineSeries[14];
    for(int i=0; i<14; i++)
    {
        seriesSadness.push_back(&seriesS[i]);
    }

    QChart *chart1=new QChart();
    chartsadness=chart1;
    for(int i=0;i<14;i++)
    {
       chart1->addSeries(seriesSadness[i]);
    }

    chart1->setBackgroundBrush(Qt::SolidPattern);
    chart1->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));

    chart1->setTitle("Sadness大类 ");
    chart1->createDefaultAxes();
    chart1->axisY()->setRange(0, 1);
    chart1->axisX()->setRange(0, 60);
    chart1->axisX()->setTitleBrush(QBrush(Qt::white));
    chart1->axisY()->setTitleBrush(QBrush(Qt::white));
    QBrush axisBrush(Qt::white);
    chart1->axisX()->setLabelsBrush(axisBrush);
    chart1->axisY()->setLabelsBrush(axisBrush);
    chart1->setTitleBrush(QBrush(Qt::white));

    chart1->axisX()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisY()->setTitleFont(QFont("Microsoft YaHei", 10, QFont::Normal, true));
    chart1->axisX()->setTitleText("时间");
    chart1->axisY()->setTitleText("表情概率");

    chart1->axisX()->setLabelsBrush(axisBrush);
    chart1->axisY()->setLabelsBrush(axisBrush);

    chart1->axisX()->setGridLineVisible(false);
    chart1->axisY()->setGridLineVisible(false);
    chart1->legend()->hide();

    chartViewseriesSadness = new ChartView(chart1);
    chartViewseriesSadness->setRenderHint(QPainter::Antialiasing);

    ui->gridLayout_Sadness->addWidget(chartViewseriesSadness);
}


void MainWindow::updateData(float value)
{
        int i;
        QVector<QPointF> oldData = series->pointsVector();
        QVector<QPointF> data;
        if (oldData.size() < 60)
        {
            data = series->pointsVector();
        } else
        {
            for (i = 1; i < oldData.size(); ++i)
            {
                data = series->pointsVector();
                data.append(QPointF(i - 1 , oldData.at(i).y()));

            }
            time++;
            init();
        }

        qint64 size = data.size();
        for(i = 0; i < 1; ++i)
        {
            data.append(QPointF(i + size, value));
        }

        series->replace(data);
        count++;
}
void MainWindow::updateDataSurprise(QList<float>value)
{
    int i;
    QList<QVector<QPointF>> oldData;
    qDebug()<<value.length();

    for(int j=0;j<value.length();j++)
    {
        oldData.append(seriesSurprise[j]->pointsVector());
    }

    QList<QVector<QPointF>>data;

    for(int j=0;j<value.length();j++)
    {
        if (oldData.at(j).size() < 61)
        {
            data.append(seriesSurprise[j]->pointsVector());
        }
        else
        {

            data.append(seriesSurprise[j]->pointsVector());
            for ( i = 1; i < oldData.at(j).size(); i++)
            {
                data[j].replace(i-1,QPointF(i - 1 , oldData[j].at(i).y()));
            }

            init();


        }
    }
    qint64 size=data.at(0).size();

    for(int i = 0; i < 1; ++i)
    {
        for(int j=0;j<value.length();j++)
        {
           data[j].append(QPointF(i + size, value.at(j)));
        }

    }

    for(int i=0;i<value.length();i++)
    {
        seriesSurprise[i]->replace(data.at(i));
    }

}
void MainWindow::updateDataSadness(QList<float>value)
{
    int i;
    QList<QVector<QPointF>> oldData;

    for(int j=0;j<value.length();j++)
    {
        oldData.append(seriesSadness[j]->pointsVector());
    }

    QList<QVector<QPointF>>data;

    for(int j=0;j<value.length();j++)
    {
        if (oldData.at(j).size() < 61)
        {
            data.append(seriesSadness[j]->pointsVector());
        }
        else
        {
            data.append(seriesSadness[j]->pointsVector());
            for ( i = 1; i < oldData.at(j).size(); i++)
            {
                data[j].replace(i-1,QPointF(i - 1 , oldData[j].at(i).y()));
            }

            init();
        }

    }
    qint64 size=data.at(0).size();

    for(int i = 0; i < 1; ++i)
    {
        for(int j=0;j<value.length();j++)
        {
           data[j].append(QPointF(i + size, value.at(j)));
        }

    }

    for(int i=0;i<value.length();i++)
    {
        seriesSadness[i]->replace(data.at(i));
    }
}
void MainWindow::updateDataJoy(QList<float>value)
{
    int i;
    QList<QVector<QPointF>> oldData;

    for(int j=0;j<value.length();j++)
    {
        oldData.append(seriesJoy[j]->pointsVector());
    }

    QList<QVector<QPointF>>data;

    for(int j=0;j<value.length();j++)
    {
        if (oldData.at(j).size() < 61)
        {
            data.append(seriesJoy[j]->pointsVector());
        }
        else
        {
            data.append(seriesJoy[j]->pointsVector());
            for ( i = 1; i < oldData.at(j).size(); i++)
            {
                data[j].replace(i-1,QPointF(i - 1 , oldData[j].at(i).y()));
            }
             init();
        }

    }
    qint64 size=data.at(0).size();

    for(int i = 0; i < 1; ++i)
    {
        for(int j=0;j<value.length();j++)
        {
           data[j].append(QPointF(i + size, value.at(j)));
        }

    }

    for(int i=0;i<value.length();i++)
    {
        seriesJoy[i]->replace(data.at(i));
    }
}
void MainWindow::updateDataAngry(QList<float>value)
{
    int i;
    QList<QVector<QPointF>> oldData;

    for(int j=0;j<value.length();j++)
    {
        oldData.append(seriesAngry[j]->pointsVector());
    }

    QList<QVector<QPointF>>data;

    for(int j=0;j<value.length();j++)
    {
        if (oldData.at(j).size() < 61)
        {
            data.append(seriesAngry[j]->pointsVector());
        }
        else
        {
            data.append(seriesAngry[j]->pointsVector());
            for ( i = 1; i < oldData.at(j).size(); i++)
            {
                data[j].replace(i-1,QPointF(i - 1 , oldData[j].at(i).y()));
            }
        }

    }
    qint64 size=data.at(0).size(); //

    for(int i = 0; i < 1; ++i)
    {
        for(int j=0;j<value.length();j++)
        {
           data[j].append(QPointF(i + size, value.at(j)));
        }

    }

    for(int i=0;i<value.length();i++)
    {
        seriesAngry[i]->replace(data.at(i));
    }
}
void MainWindow::updateDataDisgust(QList<float>value)
{
    int i;
    QList<QVector<QPointF>> oldData;

    for(int j=0;j<value.length();j++)
    {
        oldData.append(seriesDisgust[j]->pointsVector());
    }

    QList<QVector<QPointF>>data;

    for(int j=0;j<value.length();j++)
    {
        if (oldData.at(j).size() < 61)
        {
            data.append(seriesDisgust[j]->pointsVector());
        }
        else
        {
            data.append(seriesDisgust[j]->pointsVector());
            for ( i = 1; i < oldData.at(j).size(); i++)
            {
                data[j].replace(i-1,QPointF(i - 1 , oldData[j].at(i).y()));
            }
             init();
        }

    }
    qint64 size=data.at(0).size(); //

    for(int i = 0; i < 1; ++i)
    {
        for(int j=0;j<value.length();j++)
        {
           data[j].append(QPointF(i + size, value.at(j)));
        }

    }

    for(int i=0;i<value.length();i++)
    {
        seriesDisgust[i]->replace(data.at(i));
    }
}
void MainWindow::updateDataFear(QList<float>value)
{
    int i;
    QList<QVector<QPointF>> oldData;

    for(int j=0;j<value.length();j++)
    {
        oldData.append(seriesFear[j]->pointsVector());
    }

    QList<QVector<QPointF>>data;

    for(int j=0;j<value.length();j++)
    {
        if (oldData.at(j).size() < 61)
        {
            data.append(seriesFear[j]->pointsVector());
        }
        else
        {
            data.append(seriesFear[j]->pointsVector());
            for ( i = 1; i < oldData.at(j).size(); i++)
            {
                data[j].replace(i-1,QPointF(i - 1 , oldData[j].at(i).y()));
            }
             init();
        }

    }
    qint64 size=data.at(0).size(); //

    for(int i = 0; i < 1; ++i)
    {
        for(int j=0;j<value.length();j++)
        {
           data[j].append(QPointF(i + size, value.at(j)));
        }

    }

    for(int i=0;i<value.length();i++)
    {
        seriesFear[i]->replace(data.at(i));
    }
}
void MainWindow::on_pic_clicked()
{
    WId wid = 0;
    QString tempImagePath1="C:/Users/Sun/Desktop/b1/debug/weibiaoqing/noa-54960.pb";
    p = new QProcess(this);
    connect(p, SIGNAL(readyRead()),this, SLOT(readResult()));
    p->start("C:\\Users\\Sun\\Desktop\\b1\\debug\\weibiaoqing\\testgpu.exe",QStringList()<<tempImagePath1);
    timer->start();
    do
    {
      wid = (WId)FindWindow(TEXT("Main HighGUI class") ,TEXT("frame"));
    }while(wid==0);

    QWindow *m_window;
    m_window = QWindow::fromWinId(wid);
    m_window->setFlags(m_window->flags() | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    m_widget = QWidget::createWindowContainer(m_window);
    ui->gridLayout_3->addWidget(m_widget);

}


void MainWindow::readResult()
{

    QProcess *p1 = (QProcess *)sender();
    QString str;
    if(!p1->atEnd())
    {
        str+=p1->readAll();
    }
    QString result= str.simplified();
    QString y = "p";
    QString x = "i";
    QString z = ",";
    QString e= "mi";
    QString f= "mp";
    QString g=";";
    QList<int>id;
    QList<int>maxid;
    QList<float>gailv;
    int j=0;int k=0;int l=0;int m=0;int n=0,b=0;
    int i=0;
    int pos=0,pos1=0,pos2=0,pos3=0,pos4=0,pos5=0;
    while(((result.indexOf(x,j))!=-1)&&((result.indexOf(y,k)!=-1)))
    {
        j=result.indexOf(x,j);//id
        k=result.indexOf(y,k);//prob
        l=result.indexOf(z,l);//,
        m=result.indexOf(e,m);//mi
        n=result.indexOf(f,n);
        b=result.indexOf(g,b);

        pos=j;
        pos1=k;
        pos2=l;
        pos3=m;
        pos4=n;
        pos5=b;

        j=j+1;
        k=k+1;
        l=l+1;
        m=m+1;
        n=n+1;
        b=b+1;

        id.clear();
        id.append(result.mid(pos+1,pos1-pos-1).toInt());
        gailv.append(result.mid(pos1+1,pos2-pos1-1).toFloat());
        if(m!=-1)
        {
            maxid.append(result.mid(pos3+2,pos4-pos3-2).toInt());
        }
        i++;
    }

    if(gailv.length()!=0)
    {
        qDebug()<<gailv.length();
        Neutral.clear();
        Neutral.append(gailv.at(53));
        Surprise.clear();
        Surprise.append(gailv.at(50));//s a i
        Surprise.append(gailv.at(51));
        Surprise.append(gailv.at(52));
        Sadness.clear();
        for(int i=31;i<45;i++)
        {
            Sadness.append(gailv.at(i));
        }
        Joy.clear();
        for(int i=0;i<15;i++)
        {
            Joy.append(gailv.at(i));
        }
        Angry.clear();
        for(int i=15;i<22;i++)
        {
            Angry.append(gailv.at(i));
        }
        Disgust.clear();
        for(int i=22;i<31;i++)
        {
            Disgust.append(gailv.at(i));
        }
        Fear.clear();
        for(int i=45;i<50;i++)
        {
            Fear.append(gailv.at(i));
        }

    }
    emit end();

}

void MainWindow::endp()
{
    p->close();
    delete p;

}
void MainWindow::timerSlot()
{
    if(Neutral.length())
    {
        updateData(Neutral.at(0));
        updateDataSurprise(Surprise);
        updateDataSadness(Sadness);
        updateDataJoy(Joy);
        updateDataAngry(Angry);
        updateDataDisgust(Disgust);
        updateDataFear(Fear);

    }
}
